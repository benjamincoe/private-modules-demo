var config = require('@npm-partners/private-config')

module.exports = function nameAndStatus () {
  return config.name + ' is ' + config.status
}
